<?php

$lang['subversion_app_name'] = 'Subversion';
$lang['subversion_app_description'] = 'Subversion server app (which is different than a Subversion client)';
$lang['subversion_subversion_storage'] = 'Subversion Repository Storage';
