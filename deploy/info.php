<?php

/////////////////////////////////////////////////////////////////////////////
// General information
/////////////////////////////////////////////////////////////////////////////

$app['basename'] = 'subversion';
$app['version'] = '1.6.5';
$app['vendor'] = 'ClearFoundation';
$app['packager'] = 'ClearFoundation';
$app['license'] = 'GPLv3';
$app['license_core'] = 'LGPLv3';
$app['description'] = lang('subversion_app_description');
$app['powered_by'] = array(
    'packages' => array(
        'subversion' => array(
            'name' => 'Subversion',
            'url' => 'https://subversion.apache.org/',
        ),
    ),
);

/////////////////////////////////////////////////////////////////////////////
// App name and categories
/////////////////////////////////////////////////////////////////////////////

$app['name'] = lang('subversion_app_name');
$app['category'] = lang('base_category_server');
$app['subcategory'] = 'Developer'; // e.g. lang('base_subcategory_settings');
$app['menu_enabled'] = FALSE;

/////////////////////////////////////////////////////////////////////////////
// Packaging
/////////////////////////////////////////////////////////////////////////////

$app['core_only'] = TRUE;

$app['core_requires'] = array(
    'app-storage-core',
    'subversion',
);

$app['core_directory_manifest'] = array(
    '/var/clearos/subversion' => array(),
    '/var/clearos/subversion/repositories' => array(),
);

$app['core_file_manifest'] = array(
    'subversion_default.conf' => array ('target' => '/etc/clearos/storage.d/subversion_default.conf'),
);
